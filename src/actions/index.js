import axios from 'axios';

/**
 * from openweathermap.org
 * User Account Settings:
 * Username: homerj
 *    Email: homerj@mailinator.com
 * Password: oBlast1Cap
 *
 * Obtained: Mon Feb 27 14:59:43 EST 2017
 */

const API_KEY              = 'fdb0f05db128ad127e3220e453fc5e23';
const ROOT_URL             = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;
export const FETCH_WEATHER = 'FETCH_WEATHER';

/**
 * Action Creator
 * 
 * @export
 * @param {any} city 
 * @returns {Object}
 */
export function fetchWeather(city) {
	const url     = `${ROOT_URL}&q=${city},us`;
	const request = axios.get(url);

	return {
		type: FETCH_WEATHER,
		payload: request
	}
}
