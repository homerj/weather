import React, { PropTypes }                                           from 'react';
import        { Sparklines, SparklinesLine, SparklinesReferenceLine } from 'react-sparklines';
import _ from 'lodash';

function average(data) {
	return _.round(_.sum(data)/data.length)
}

const Chart = (props) => {
	return(
		<div>
			<Sparklines height={120} width={180} data={props.data}>
				<SparklinesLine color={props.color}/>
				<SparklinesReferenceLine type="avg"/>
				<div>{average(props.data)} {props.units}</div>
			</Sparklines>
		</div>
	)
};


Chart.propTypes = {
	data: PropTypes.object.isRequired,
	color: PropTypes.string.isRequired,
	units: PropTypes.string.isRequired
};

export default Chart;
